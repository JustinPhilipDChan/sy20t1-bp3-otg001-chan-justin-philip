#include<iostream>
#include<cstdlib>
#include<string>
#include<time.h>
#include "Unit.h"
#include "Player.h"
#include "Monster.h"

using namespace std;

//QUALITY OF LIFE
void charDesign();
void barDesign();



int main()
{
	srand(time(NULL));

	Player* player = new Player();
	Unit* unit = (Unit*)player;

	string name;
	int inputClass = 0;
	int inputChoices;
	int inputMove;
	int battleChoice;
	int runAwayChance;

	int spawnChance;

	//CHAR CREATION
	charDesign();
	cout << "Input player name: ";
	getline(cin, name);
	unit->setName(name);
		do {
			cout << endl << endl << "Input [1] for Warrior [2] for Assassin [3] for Archer [4] for Tank." << endl << "Choose your class:";
				cin >> inputClass;
		} while ((inputClass >= 5) && (inputClass <= 0));

	unit->selectClass(inputClass);

	system("pause"); system("cls");
		
		// GAMEPLAY
		do {
			//do while to force player to input 1 2 3 4
			do {
				player->getLocation();
				cout << "1.) Move\n2.) Rest\n3.) View Stats\n4.) Quit\nChoose what you will do: ";
				cin >> inputChoices;
			} while ((inputChoices >= 5) && (inputChoices <= 0));
			
			if (inputChoices == 4)
			{
				return 0;
			}
			else if (inputChoices == 3)
			{
				barDesign();
				player->viewStats();
				cout << endl;
				system("pause"); system("cls");
			}
			else if (inputChoices == 2)
			{
				barDesign();
				player->rest(unit);
				cout << endl;
				system("pause"); system("cls");
			}
			else if (inputChoices == 1)
			{
				//do while to force player to input 1 2 3 4 
				do {
					barDesign();
					cout << "1.) North 2.) South 3.) East 4.) West : ";
					cin >> inputMove;
				} while ((inputMove >= 5) && (inputMove <= 0));
				player->move(inputMove);

				//Spawn Chance out of 100
				spawnChance = rand() % 100 + 1;

				if (spawnChance > 0 && spawnChance <= 20)
				{
					break;
				}
				else if (spawnChance > 20 && spawnChance <= 45)
				{
					Unit* goblinUnit = new Monster("Goblin", "Goblin", 0, 3, 3, 2, 2);

					cout << "A " << goblinUnit->getName() << " has spawned!\nPrepare for combat!!!\n";
					system("pause"); system("cls");
					
					//repeating attack do while function
					do {
						//do while to ensure player chooses 1 or 2 
						do {
							cout << unit->getName() << " HP: " << unit->getHp() << "/" << unit->getBaseHp() << endl;
							cout << goblinUnit->getName() << " HP: " << goblinUnit->getHp() << "/" << goblinUnit->getBaseHp() << endl << endl;

							cout << "1.) Attack\n2.) Run Away\nWhat do you want to do?   ";
							cin >> battleChoice;
						} while (battleChoice >= 3 && battleChoice <= 0);

						if (battleChoice == 1) {
							unit->attack(goblinUnit);
							goblinUnit->attack(unit);
						}
						else if (battleChoice == 2) {
							runAwayChance = rand() % 100 + 1;

							if (runAwayChance >= 0 && runAwayChance <= 25) {
								cout << "Successfully ran away!\n";
								break;
							}
							else {
								goblinUnit->attack(unit);
							}
						}

						if (unit->getHp() <= 0) break; if (goblinUnit->getHp() <= 0) break;

					} while ((goblinUnit->getHp() >= 0) || (unit->getHp() >= 0));

					//get exp
					if (goblinUnit->getHp() <= 0) player->getExp(goblinUnit);

					//DELETE
					delete goblinUnit;
				}
				else if (spawnChance > 45 && spawnChance <= 70)
				{
					Unit* ogreUnit = new Monster("Ogre", "Ogre", 10, 5, 8, 4, 4);
					cout << "An " << ogreUnit->getName() << " has spawned!\nPrepare for combat!!!\n";
					system("pause"); system("cls");
					do {
						do {
							cout << unit->getName() << " HP: " << unit->getHp() << "/" << unit->getBaseHp() << endl;
							cout << ogreUnit->getName() << " HP: " << ogreUnit->getHp() << "/" << ogreUnit->getBaseHp() << endl << endl;

							cout << "1.) Attack\n2.) Run Away\nWhat do you want to do?   ";
							cin >> battleChoice;
						} while (battleChoice >= 3 && battleChoice <= 0);

						if (battleChoice == 1) {
							unit->attack(ogreUnit);
							ogreUnit->attack(unit);
						}
						else if (battleChoice == 2) {
							runAwayChance = rand() % 100 + 1;

							if (runAwayChance >= 0 && runAwayChance <= 25) {
								cout << "Successfully ran away!\n";
								break;
							}
							else {
								ogreUnit->attack(unit);
							}
						}
						if (unit->getHp() <= 0) break; if (ogreUnit->getHp() <= 0) break;
					} while ((ogreUnit->getHp() >= 0) || (unit->getHp() >= 0));

					if (ogreUnit->getHp() <= 0) player->getExp(ogreUnit);
					delete ogreUnit;
				}
				else if (spawnChance > 70 && spawnChance <= 95)
				{
					Unit* orcUnit = new Monster("Orc", "Orc", 20, 15, 10, 10, 10);

					cout << "An " << orcUnit->getName() << " has spawned!\nPrepare for combat!!!\n";
					system("pause"); system("cls");
					//repeating attack do while function
					do {
						//do while to ensure player chooses 1 or 2 
						do {
							cout << unit->getName() << " HP: " << unit->getHp() << "/" << unit->getBaseHp() << endl;
							cout << orcUnit->getName() << " HP: " << orcUnit->getHp() << "/" << orcUnit->getBaseHp() << endl << endl;

							cout << "1.) Attack\n2.) Run Away\nWhat do you want to do?   ";
							cin >> battleChoice;
						} while (battleChoice >= 3 && battleChoice <= 0);

						if (battleChoice == 1) {
							unit->attack(orcUnit);
							orcUnit->attack(unit);
						}
						else if (battleChoice == 2) {
							runAwayChance = rand() % 100 + 1;

							if (runAwayChance >= 0 && runAwayChance <= 25) {
								cout << "Successfully ran away!\n";
								break;
							}
							else {
								orcUnit->attack(unit);
							}
						}
						if (unit->getHp() <= 0) break; if (orcUnit->getHp() <= 0) break;
					} while ((orcUnit->getHp() >= 0) || (unit->getHp() >= 0));
					if (orcUnit->getHp() <= 0) player->getExp(orcUnit);
					delete orcUnit;
				}
				else if (spawnChance > 95 && spawnChance <= 100)
				{
					Unit* orcLordUnit = new Monster("Orc Lord", "Orc Lord", 50, 30, 30, 20, 25);

					cout << "An " << orcLordUnit->getName() << " has spawned!\nPrepare for combat!!!\n";
					system("pause"); system("cls");
					do {
						do {
							cout << unit->getName() << " HP: " << unit->getHp() << "/" << unit->getBaseHp() << endl;
							cout << orcLordUnit->getName() << " HP: " << orcLordUnit->getHp() << "/" << orcLordUnit->getBaseHp() << endl << endl;

							cout << "1.) Attack\n2.) Run Away\nWhat do you want to do?   ";
							cin >> battleChoice;
						} while (battleChoice >= 3 && battleChoice <= 0);

						if (battleChoice == 1) {
							unit->attack(orcLordUnit);
							orcLordUnit->attack(unit);
						}
						else if (battleChoice == 2) {
							runAwayChance = rand() % 100 + 1;

							if (runAwayChance >= 0 && runAwayChance <= 25) {
								cout << "Successfully ran away!\n";
								break;
							}
							else {
								orcLordUnit->attack(unit);
							}
							
						}
						if (unit->getHp() <= 0) break; if (orcLordUnit->getHp() <= 0) break;
					} while ((orcLordUnit->getHp() >= 0) || (unit->getHp() >= 0));
					if (orcLordUnit->getHp() <= 0) player->getExp(orcLordUnit);
					delete orcLordUnit;
				}

				if (player->getCurrentXP() > player->getRequiredXP()) player->levelUp(player);
			}

			if (unit->getHp() <= 0)
			{
				cout << "YOU DIED.\n";
				return 0;
			}
		} while (unit->getHp() >= 0);

		delete unit;
}

void charDesign()
{
	cout << "CHARACTER CREATION\n~~~~~~~~~~~~~~~~~~\n";
}

void barDesign()
{
	cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
}
