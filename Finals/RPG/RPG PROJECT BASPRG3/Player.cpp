#include "Player.h"
#include "Unit.h"
#include <iostream>
#include <cstdlib>

Player::Player()
	: Unit("Player", "Player Class", 0, 0, 0, 0, 0)
{
	x = 0;
	y = 0;
}

Player::~Player()
{
}

int Player::getCurrentXP()
{
	return mCurrentXP;
}

int Player::getLevel()
{
	return mLevel;
}

int Player::getRequiredXP()
{
	return mRequiredXP;
}

void Player::viewStats()
{
	cout << "Name = " << this->getName() << endl
		<< "Level = " << mLevel << endl
		<< "Class = " << this->mPlayerClass << endl
		<< "Hp = " << this->getHp() << endl
		<< "Pow = " << this->getPow() << endl
		<< "Vit = " << this->getVit() << endl
		<< "Agi = " << this->getAgi() << endl
		<< "Dex = " << this->getDex() << endl << endl
		<< "EXP = " << mCurrentXP << "/" << mRequiredXP << endl;
}

void Player::move(int input)
{
	//NSEW FORMAT

	if (input == 1)
	{
		x = x + 1;
	}
	else if (input == 2)
	{
		x = x - 1;
	}
	else if (input == 3)
	{
		y = y + 1;
	}
	else if (input == 4)
	{
		y = y - 1;
	}
}

void Player::getLocation()
{
	cout << "Player location: (" << x << " , " << y << ")" << endl;
}


void Player::selectClass(int input)
{
	//HARD CODE 1 2 3 4 CLASS AND SET VALUES

	if (input == 1)
	{
		this->setPlayerClass("Warrior");
		this->setHp(20);
		this->setBaseHp(20);
		this->setPow(8);
		this->setVit(5);
		this->setAgi(2);
	}
	else if (input == 2)
	{
		this->setPlayerClass("Assassin");
		this->setHp(-5);
		this->setBaseHp(-5);
		this->setPow(10);
		this->setVit(-4);
		this->setAgi(15);
		this->setDex(5);
	}
	else if (input == 3)
	{
		this->setPlayerClass("Archer");
		this->setHp(-15);
		this->setBaseHp(-15);
		this->setPow(7);
		this->setVit(-5);
		this->setDex(15);
		this->setAgi(5);
	}
	else if (input == 4)
	{
		this->setPlayerClass("Tank");
		this->setHp(40);
		this->setBaseHp(40);
		this->setPow(3);
		this->setVit(10);
		this->setAgi(-10);
		this->setDex(-5);
	}
}

void Player::getExp(Unit* target)
{
	if (target->getName() == "Goblin")
	{
		mCurrentXP = mCurrentXP + 100;
	}
	else if (target->getName() == "Ogre")
	{
		mCurrentXP = mCurrentXP + 250;
	}
	else if (target->getName() == "Orc")
	{
		mCurrentXP = mCurrentXP + 500;
	}
	else if (target->getName() == "Orc Lord")
	{
		mCurrentXP = mCurrentXP + 1000;
	}
}

void Player::levelUp(Player* player)
{
	mCurrentXP = mCurrentXP - mRequiredXP; //hopefully its always positive

	mLevel = mLevel + 1; // since required xp is linked to level * 100, this automatically raise required xp
	

	cout << endl << "TU-TU-RUUUUUUUU~~~~ You leveled up!!!!!" << endl << endl;
	
	//RANDOM STAT GIVER 0 - 5
	player->setBaseHp(rand() % 6);
	player->setPow(rand() % 6);
	player->setVit(rand() % 6);
	player->setDex(rand() % 6);
	player->setDex(rand() % 6);
}

void Player::rest(Unit* player)
{
	player->setHp(player->getBaseHp());

	if (player->getHp() > player->getBaseHp()) {
		mHp = mBaseHp;
	}

	cout << this->getName() << " is resting...\n";
}
