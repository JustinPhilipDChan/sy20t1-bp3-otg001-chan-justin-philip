#include "Unit.h"
#include <iostream>
#include <cstdlib>

Unit::Unit(string name, string playerClass, int hp, int pow, int vit, int agi, int dex)
{
	mName = name;
	mPlayerClass = playerClass;
	mHp = 30 + hp;
	mBaseHp = 30 + hp;
	mPow = 5 + pow;
	mVit = 5 + vit;
	mAgi = 5 + agi;
	mDex = 5 + dex;
}

string Unit::getName()
{
	return mName;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getBaseHp()
{
	return mBaseHp;
}

string Unit::getClass()
{
	return mPlayerClass;
}

int Unit::getPow()
{
	return mPow;
}

int Unit::getVit()
{
	return mVit;
}

int Unit::getAgi()
{
	return mAgi;
}

int Unit::getDex()
{
	return mDex;
}

void Unit::setName(string name)
{
	mName = name;
}

void Unit::setPlayerClass(string job)
{
	mPlayerClass = job;
}

void Unit::setHp(int value)
{
	mHp = mHp + value;
}

void Unit::minusHp(int value)
{
	mHp = mHp - value;

	if (mHp <= 0) mHp = 0;
	if (mHp > mBaseHp) mHp = mBaseHp;
}

void Unit::setBaseHp(int value)
{
	mBaseHp = mBaseHp + value;
}

void Unit::setPow(int value)
{
	mPow = mPow + value;
}

void Unit::setVit(int value)
{
	mVit = mVit + value;
}

void Unit::setAgi(int value)
{
	mAgi = mAgi + value;
}

void Unit::setDex(int value)
{
	mDex = mDex + value;
}

void Unit::attack(Unit* target)
{
	int damage = (mPow - target->getVit());

	if (damage <= 0) damage = 1; // damage will always be above 0

	int hitRate = ((int) this->getDex() / (int) target->getAgi()) * 100;

	//clamp hitrate to 20-80
	if (hitRate < 20) hitRate = 20;
	else if (hitRate > 80) hitRate = 80;

	int actualHitChance = rand() % 80 + 1;
	//clamp hit chance to 20-80
	if (actualHitChance < 20) actualHitChance = 20;

	//HIT CHANCE BEFORE ATTACK HAPPENS
	//lower = hit
	//higher = miss
	if (actualHitChance >= hitRate) { 

		//if (target->getBaseHp() < damage);
	cout << mName << " did " << damage << " damage to " << target->getName() << "!\n\n";

	target->minusHp(damage);
	}
	else {
		cout << mName << " missed!!\n\n";
	}
}

void Unit::selectClass(int input)
{

}
