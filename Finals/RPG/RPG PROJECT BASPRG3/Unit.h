#include <string>

using namespace std;

#pragma once
class Unit
{
public:
	Unit(string name, string playerClass, int hp, int pow, int vit, int agi, int dex); // Constructor

	// Getters
	string getName();
	int getHp();
	int getBaseHp();
	string getClass();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	// Setters
	void setName(string name);
	void setPlayerClass(string job); // doesnt work
	// Set = value + base (10)
	void setHp(int value);
	void minusHp(int value);
	void setBaseHp(int value);
	void setPow(int value);
	void setVit(int value);
	void setAgi(int value);
	void setDex(int value);

	//virtual for both Player and Monster
	void attack(Unit* target);

	//virtual for Player to create class
	virtual void selectClass(int input);

protected:
	int mHp; //Health points. Once it drops to zero, unit dies.
	int mBaseHp;
	string mPlayerClass;    // private player class

private:

	string mName;

	int mPow; //Power. Each point directly adds 1 point to base damage.

	int mVit;	//Vitality. Each point reduces 1 point from damage received. This is applied on the base damage (before computing the 50% bonus damage, if applicable)
				//damage = (POW of attacker - VIT of defender) * bonusDamage

	int mAgi; //Agility. Determines evasion rate. Decreases the likelihood of getting hit. Each point decreases the attacker�s hit rate.

	int mDex; //Dexterity. Determines hit rate or chance of getting your attacks to land successfully.
};

