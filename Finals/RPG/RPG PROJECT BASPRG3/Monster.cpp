#include "Monster.h"
#include "Unit.h"


Monster::Monster()
	: Unit("Monster", "Monster", 0, 0, 0, 0, 0)
{
}

Monster::Monster(string name, string playerClass, int hp, int pow, int vit, int agi, int dex)
	: Unit(name, playerClass, hp, pow, vit, agi, dex)
{
}

Monster::~Monster()
{
}
