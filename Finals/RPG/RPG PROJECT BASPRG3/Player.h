#pragma once
#include "Unit.h"
#include <string>

using namespace std;

class Player : public Unit
{
public:
	Player();
	~Player();

	//getters
	int getCurrentXP();
	int getLevel();
	int getRequiredXP();

	//view stats
	void viewStats();

	void move(int input);

	void getLocation();

	void selectClass(int input) override;

	void getExp(Unit* target);

	void levelUp(Player* player);

	void rest(Unit* player);

private:

	int mLevel = 1; // level

	int mCurrentXP = 0; // exp

	int mRequiredXP = (mLevel * 1000); //REQUIRED EXP WILL ALWAYS BE LEVEL * 1000

	//map coordinates
	int x;
	int y;
};