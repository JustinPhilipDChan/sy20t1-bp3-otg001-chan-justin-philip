#include "Planet.h"
#include <iostream>

//CUBE CONSTRUCTOR CODE BELOW

//Planet::Planet(SceneManager* SceneMgr, float size, ColourValue colour, std::string name)
//{
//	mSceneMgr = SceneMgr;
//	ManualObject* manual = mSceneMgr->createManualObject();
//
//	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");
//
//	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
//	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);
//	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.1, 0.1, 0.1, 0);
//
//	//sun will always shine and is not affected by light
//	if (name == "sun") {
//	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
//	}
//	else
//	{
//		manual->begin(name, RenderOperation::OT_TRIANGLE_LIST);
//	}
//	
//	
//	mRevolutionSpd = 1;
//
//	
//	//front
//	manual->position(-size / 2, -size / 2, size / 2); //0
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, 1));
//	manual->position(size / 2, -size / 2, size / 2); //1
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, 1));
//	manual->position(size / 2, size / 2, size / 2); //2
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, 1));
//	manual->position(-size / 2, size / 2, size / 2); //3
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, 1));
//
//	//back
//	manual->position(-size / 2, -size / 2, -size / 2); //4
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, -1));
//	manual->position(size / 2, -size / 2, -size / 2); //5
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, -1));
//	manual->position(size / 2, size / 2, -size / 2); //6
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, -1));
//	manual->position(-size / 2, size / 2, -size / 2); //7
//	manual->colour(colour);
//	manual->normal(Vector3(0, 0, -1));
//
//	//right
//	manual->position(size / 2, -size / 2, size / 2); //8
//	manual->colour(colour);
//	manual->normal(Vector3(1, 0, 0));
//	manual->position(size / 2, -size / 2, -size / 2); //9
//	manual->colour(colour);
//	manual->normal(Vector3(1, 0, 0));
//	manual->position(size / 2, size / 2, size / 2); //10
//	manual->colour(colour);
//	manual->normal(Vector3(1, 0, 0));
//	manual->position(size / 2, size / 2, -size / 2); //11
//	manual->colour(colour);
//	manual->normal(Vector3(1, 0, 0));
//
//	//left
//	manual->position(-size / 2, -size / 2, -size / 2); //12
//	manual->colour(colour);
//	manual->normal(Vector3(-1, 0, 0));
//	manual->position(-size / 2, -size / 2, size / 2); //13
//	manual->colour(colour);
//	manual->normal(Vector3(-1, 0, 0));
//	manual->position(-size / 2, size / 2, -size / 2); //14
//	manual->colour(colour);
//	manual->normal(Vector3(-1, 0, 0));
//	manual->position(-size / 2, size / 2, size / 2); //15
//	manual->colour(colour);
//	manual->normal(Vector3(-1, 0, 0));
//
//	//top
//	manual->position(-size / 2, size / 2, size / 2); //16
//	manual->colour(colour);
//	manual->normal(Vector3(0, 1, 0));
//	manual->position(size / 2, size / 2, size / 2); //17
//	manual->colour(colour);
//	manual->normal(Vector3(0, 1, 0));
//	manual->position(size / 2, size / 2, -size / 2); //18
//	manual->colour(colour);
//	manual->normal(Vector3(0, 1, 0));
//	manual->position(-size / 2, size / 2, -size / 2); //19
//	manual->colour(colour);
//	manual->normal(Vector3(0, 1, 0));
//
//	//bottom
//	manual->position(-size / 2, -size / 2, -size / 2); //20
//	manual->colour(colour);
//	manual->normal(Vector3(0, -1, 0));
//	manual->position(size / 2, -size / 2, -size / 2); //21
//	manual->colour(colour);
//	manual->normal(Vector3(0, -1, 0));
//	manual->position(size / 2, -size / 2, size / 2); //22
//	manual->colour(colour);
//	manual->normal(Vector3(0, -1, 0));
//	manual->position(-size / 2, -size / 2, size / 2); //23
//	manual->colour(colour);
//	manual->normal(Vector3(0, -1, 0));
//
//
//	//front
//	manual->index(0);
//	manual->index(1);
//	manual->index(2);
//
//	manual->index(0);
//	manual->index(2);
//	manual->index(3);
//
//	//back
//	manual->index(5);
//	manual->index(7);
//	manual->index(6);
//
//	manual->index(5);
//	manual->index(4);
//	manual->index(7);
//
//	//left
//	manual->index(12);
//	manual->index(13);
//	manual->index(14);
//
//	manual->index(15);
//	manual->index(14);
//	manual->index(13);
//
//	//right
//	manual->index(8);
//	manual->index(9);
//	manual->index(10);
//
//	manual->index(11);
//	manual->index(10);
//	manual->index(9);
//
//	//top
//	manual->index(16);
//	manual->index(17);
//	manual->index(18);
//
//	manual->index(16);
//	manual->index(18);
//	manual->index(19);
//
//	//bottom
//	manual->index(20);
//	manual->index(21);
//	manual->index(22);
//
//	manual->index(20);
//	manual->index(22);
//	manual->index(23);
//
//	manual->end();
//	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
//	mNode->attachObject(manual);
//}

//SPHERE CONSTRUCTOR
Planet::Planet(SceneManager* SceneMgr, const float r, ColourValue colour, std::string name)
{
	//Code taken from http://wiki.ogre3d.org/ManualSphereMeshes?fbclid=IwAR3_Od8TnzI8AkznV0tBlmZgl670zgJgkc7N-ljQPeKxg-EEz9V7BP9LNqk#Creating_a_sphere_with_ManualObject
	// Modified code to fit the requirements

	mSceneMgr = SceneMgr;

	int nRings = 20;
	int nSegments = 20;

	mRevolutionSpd = 1;

	ManualObject* manual = mSceneMgr->createManualObject(name);
	
	MaterialPtr myManualObjectMaterial = Ogre::MaterialManager::getSingleton().create(name, "General");

	myManualObjectMaterial->getTechnique(0)->setLightingEnabled(true);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setDiffuse(colour);
	myManualObjectMaterial->getTechnique(0)->getPass(0)->setSpecular(0.5f, 0.5f, 0.5f, 0);

	manual->begin(name, RenderOperation::OT_TRIANGLE_LIST);

	float fDeltaRingAngle = (Math::PI / nRings);
	float fDeltaSegAngle = (2 * Math::PI / nSegments);
	unsigned short wVerticeIndex = 0;

	// Generate the group of rings for the sphere
	for (int ring = 0; ring <= nRings; ring++) {
		float r0 = r * sinf(ring * fDeltaRingAngle);
		float y0 = r * cosf(ring * fDeltaRingAngle);

		// Generate the group of segments for the current ring
		for (int seg = 0; seg <= nSegments; seg++) {
			float x0 = r0 * sinf(seg * fDeltaSegAngle);
			float z0 = r0 * cosf(seg * fDeltaSegAngle);

			// Add one vertex to the strip which makes up the sphere
			manual->position(x0, y0, z0);
			manual->colour(colour);		// sets the color per vertex
			manual->normal(Vector3(x0, y0, z0).normalisedCopy());
			manual->textureCoord((float)seg / (float)nSegments, (float)ring / (float)nRings);

			if (ring != nRings) {
				// each vertex (except the last) has six indicies pointing to it
				manual->index(wVerticeIndex + nSegments + 1);
				manual->index(wVerticeIndex);
				manual->index(wVerticeIndex + nSegments);
				manual->index(wVerticeIndex + nSegments + 1);
				manual->index(wVerticeIndex + 1);
				manual->index(wVerticeIndex);
				wVerticeIndex++;
			}
		}; // end for seg
	} // end for ring
	manual->end();

	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(manual);

	// Not sure if mesh is needed for this Project(3) but i'll keep it here	
	MeshPtr mesh = manual->convertToMesh(name);
	mesh->_setBounds(AxisAlignedBox(Vector3(-r, -r, -r), Vector3(r, r, r)), false);

	mesh->_setBoundingSphereRadius(r);
	unsigned short src, dest;
	if (!mesh->suggestTangentVectorBuildParams(VES_TANGENT, src, dest))
	{
		mesh->buildTangentVectors(VES_TANGENT, src, dest);
	}
}

void Planet::setRevolutionSpeed(float revolutionSpd)
{
	mRevolutionSpd += revolutionSpd;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

SceneNode* Planet::getNode()
{
	return mNode;
}

float Planet::getRevolutionSpd()
{
	return mRevolutionSpd;
}

void Planet::update(const FrameEvent& evt)
{
	Degree revolutionDegrees = Degree(mRevolutionSpd * evt.timeSinceLastFrame);

	//MOON NOT REVOLVING AROUND EARTH
	float oldX = mNode->getPosition().x - mParent->getNode()->getPosition().x; 
	float oldZ = mNode->getPosition().z - mParent->getNode()->getPosition().z; //IDK WHERE MOON IS REVOLVING

	float newX = (oldX * Math::Cos(Radian(revolutionDegrees)) + (oldZ * Math::Sin(Radian(revolutionDegrees)))) + mParent->getNode()->getPosition().x;
	float newZ = (oldX * -(Math::Sin(Radian(revolutionDegrees))) + (oldZ * Math::Cos(Radian(revolutionDegrees)))) + mParent->getNode()->getPosition().z;

	mNode->setPosition(newX, mNode->getPosition().y, newZ);


	//make planets rotate at Y axis
	Radian rotationAngle = Radian(Degree(mRevolutionSpd * evt.timeSinceLastFrame));
	mNode->rotate(Vector3(0, 1, 0), rotationAngle);
}