#pragma once
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>

using namespace Ogre;

class Planet 
{
public:
	//Planet(SceneManager* SceneMgr, float size, ColourValue colour, std::string name);
	Planet(SceneManager* SceneMgr, const float r, ColourValue colour, std::string name);
	

	//setters and getters
	void setRevolutionSpeed(float revolutionSpd);
	float getRevolutionSpd();
	void setParent(Planet* parent);
	SceneNode* getNode();

	void update(const FrameEvent& evt);
	

private:
	SceneNode* mNode;
	SceneManager* mSceneMgr;

	Planet* mParent;

	float mRevolutionSpd;
};

