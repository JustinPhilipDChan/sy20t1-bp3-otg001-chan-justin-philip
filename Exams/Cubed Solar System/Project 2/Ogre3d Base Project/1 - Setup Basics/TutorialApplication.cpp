/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	//earth is 6 degrees per second (365 days)
	//mercury revolves around the sun in 88 days, mercury revolves 4.15 faster than earth (365 / 88)
	//using that logic, calculate the rest of the planets
	
	//createLight function below the frameStarted function
	createLight(ColourValue(0.5f, 0.5f, 0), ColourValue(1.0f, 1.0f, 1.0f));

	//mSceneMgr->setAmbientLight(ColourValue(0.5f, 0.5f, 0.5)); //high intensity
	
	sun = new Planet(mSceneMgr, 30, ColourValue(1, 1, 0,0), "BaseWhiteNoLighting");
	sun->setParent(sun);

	mercury = new Planet(mSceneMgr, 4.8, ColourValue(0.82f, 0.5f, 0.34f,0), "mercury");
	mercury->getNode()->setPosition(40, 0, 0);
	mercury->setParent(sun);
	mercury->setRevolutionSpeed(24.9f);

	venus = new Planet(mSceneMgr, 12, ColourValue(0.83f, 0.9f, 0.67f, 0), "venus");
	venus->getNode()->setPosition(60, 0, 0);
	venus->setParent(sun);
	venus->setRevolutionSpeed(9.78f);

	earth = new Planet(mSceneMgr, 15, ColourValue(0,0,1,0), "earth");
	earth->getNode()->setPosition(80, 0, 0);
	earth->setParent(sun);
	earth->setRevolutionSpeed(6);

	moon = new Planet(mSceneMgr, 2, ColourValue(0.7f, 0.7f, 0.7f,0), "moon");
	moon->getNode()->setPosition(90, 0, 0);
	moon->setParent(earth); // i dont know where the moon is revolving
	moon->setRevolutionSpeed(12);

	mars = new Planet(mSceneMgr, 11, ColourValue(0.71f, 0.25f, 0.05f,0), "mars");
	mars->getNode()->setPosition(120, 0, 0);
	mars->setParent(sun);
	mars->setRevolutionSpeed(3.18f);
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	sun->update(evt);
	mercury->update(evt);
	venus->update(evt);
	moon->update(evt);
	earth->update(evt);
	mars->update(evt);





	// Below are the codes from the previous exercises

	// { Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	//	cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);

	//Degree revolutionDegrees = Degree(30 * evt.timeSinceLastFrame);

	//float newX = (cubeNode->getPosition().x * Math::Cos(Radian(revolutionDegrees))) + (cubeNode->getPosition().z * Math::Sin(Radian(revolutionDegrees)));
	//float newZ = (cubeNode->getPosition().x * -(Math::Sin(Radian(revolutionDegrees)))) + (cubeNode->getPosition().z * Math::Cos(Radian(revolutionDegrees)));

	//cubeNode->setPosition(newX, cubeNode->getPosition().y, newZ);

	////ROTATION

	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8)) //X+
	//{
	//	cubeNode->rotate(Vector3(1, 0, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2)) //X-
	//{
	//	cubeNode->rotate(Vector3(-1, 0, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6)) //Y+
	//{
	//	cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)) //Y-
	//{
	//	cubeNode->rotate(Vector3(0, -1, 0), rotationAngle);
	//}

	////STOP ROTATION
	//if (!(mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)))
	//{
	//	cubeNode->rotate(Vector3(0, 0, 0), rotationAngle);
	//}



	////MOVEMENT
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
	//{
	//	//cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame , 0.0, 0.0);
	//	//mAcceleration = mAcceleration + 0.2;
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;

	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	////diagonal input
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}

	//// if nothing is pressed, reset acceleration
	//if (!mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_I) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	velocity = Vector3::ZERO;
	//}


	//speed++;
	//cubeNode->translate(velocity);
	//}
	return true;
}

void TutorialApplication::createLight(ColourValue diffuse, ColourValue spec)
{
	//light is at 0,0 while every cube is revolving around 0,0 except for SUN which is 0,0 itself
	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setDiffuseColour(diffuse); // currently yellow (1,1,0)
	pointLight->setSpecularColour(spec);
	pointLight->setAttenuation(3250, 1.0f, 0.014, 0.00007); // adjusted intensity, values got from the Wiki
	pointLight->setCastShadows(false);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
