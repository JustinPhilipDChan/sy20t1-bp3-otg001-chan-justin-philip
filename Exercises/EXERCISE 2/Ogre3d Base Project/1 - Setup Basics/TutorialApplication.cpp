/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject* cube = createCube(10.0);
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(cube);
}

ManualObject* TutorialApplication::createCube(float size)
{
	ManualObject* manual = mSceneMgr->createManualObject("");
	manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);

	manual->position(-size / 2, -size / 2, size / 2); //0
	manual->position(size / 2, -size / 2, size / 2); //1
	manual->position(size / 2, size / 2, size / 2); //2
	manual->position(-size / 2, size / 2, size / 2); //3
	manual->position(-size / 2, -size / 2, -size / 2); //4
	manual->position(size / 2, -size / 2, -size / 2); //5
	manual->position(size / 2, size / 2, -size / 2); //6
	manual->position(-size / 2, size / 2, -size / 2); //7

	//front
	manual->index(0);
	manual->index(1);
	manual->index(2);

	manual->index(0);
	manual->index(2);
	manual->index(3);

	//back
	manual->index(5);
	manual->index(7);
	manual->index(6);

	manual->index(5);
	manual->index(4);
	manual->index(7);

	//left
	manual->index(4);
	manual->index(3);
	manual->index(7);

	manual->index(4);
	manual->index(0);
	manual->index(3);

	//right
	manual->index(1);
	manual->index(6);
	manual->index(2);

	manual->index(1);
	manual->index(5);
	manual->index(6);

	//top
	manual->index(3);
	manual->index(6);
	manual->index(7);

	manual->index(3);
	manual->index(2);
	manual->index(6);

	//bottom
	manual->index(0);
	manual->index(5);
	manual->index(4);

	manual->index(0);
	manual->index(1);
	manual->index(5);

	manual->end();
	return manual;
}

//didnt use
//bool TutorialApplication::keyPressed(const OIS::KeyEvent& arg)
//{
//	return true;
//}
//bool TutorialApplication::keyReleased(const OIS::KeyEvent& arg)
//{
//	return true;
//}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
	{
		cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame , 0.0, 0.0);
		mAcceleration = mAcceleration + 0.2;
		
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
	{
		cubeNode->translate(-10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0, 0.0);
		mAcceleration = mAcceleration + 0.2;
		
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		cubeNode->translate(0.0, 10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
	
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		cubeNode->translate(0.0, -10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
		
	}
	//diagonal input
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame, 10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame, -10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		cubeNode->translate(-10.0 * mAcceleration * evt.timeSinceLastFrame, 10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
	}
	else if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		cubeNode->translate(-10.0 * mAcceleration * evt.timeSinceLastFrame, -10.0 * mAcceleration * evt.timeSinceLastFrame, 0.0);
		mAcceleration = mAcceleration + 0.2;
	}

	// if nothing is pressed, reset acceleration
	if (!mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_I) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		resetAcceleration();
	}

	return true;
}
void TutorialApplication::resetAcceleration()
{
	mAcceleration = 0.2;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
