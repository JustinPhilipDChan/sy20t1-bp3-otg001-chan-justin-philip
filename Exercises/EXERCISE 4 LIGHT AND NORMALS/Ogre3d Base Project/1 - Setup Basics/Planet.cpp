#include "Planet.h"
#include <iostream>


Planet::Planet(SceneManager* SceneMgr, float size, ColourValue colour)
{
	mSceneMgr = SceneMgr;
	ManualObject* manual = mSceneMgr->createManualObject();
	manual->begin("BaseWhite", RenderOperation::OT_TRIANGLE_LIST);
	
	mRevolutionSpd = 1;

	
	//front
	manual->position(-size / 2, -size / 2, size / 2); //0
	manual->colour(colour);
	manual->normal(Vector3(0, 0, 1));
	manual->position(size / 2, -size / 2, size / 2); //1
	manual->colour(colour);
	manual->normal(Vector3(0, 0, 1));
	manual->position(size / 2, size / 2, size / 2); //2
	manual->colour(colour);
	manual->normal(Vector3(0, 0, 1));
	manual->position(-size / 2, size / 2, size / 2); //3
	manual->colour(colour);
	manual->normal(Vector3(0, 0, 1));

	//back
	manual->position(-size / 2, -size / 2, -size / 2); //4
	manual->colour(colour);
	manual->normal(Vector3(0, 0, -1));
	manual->position(size / 2, -size / 2, -size / 2); //5
	manual->colour(colour);
	manual->normal(Vector3(0, 0, -1));
	manual->position(size / 2, size / 2, -size / 2); //6
	manual->colour(colour);
	manual->normal(Vector3(0, 0, -1));
	manual->position(-size / 2, size / 2, -size / 2); //7
	manual->colour(colour);
	manual->normal(Vector3(0, 0, -1));

	//right
	manual->position(size / 2, -size / 2, size / 2); //8
	manual->colour(colour);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, -size / 2, -size / 2); //9
	manual->colour(colour);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, size / 2, size / 2); //10
	manual->colour(colour);
	manual->normal(Vector3(1, 0, 0));
	manual->position(size / 2, size / 2, -size / 2); //11
	manual->colour(colour);
	manual->normal(Vector3(1, 0, 0));

	//left
	manual->position(-size / 2, -size / 2, -size / 2); //12
	manual->colour(colour);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, -size / 2, size / 2); //13
	manual->colour(colour);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, size / 2, -size / 2); //14
	manual->colour(colour);
	manual->normal(Vector3(-1, 0, 0));
	manual->position(-size / 2, size / 2, size / 2); //15
	manual->colour(colour);
	manual->normal(Vector3(-1, 0, 0));

	//top
	manual->position(-size / 2, size / 2, size / 2); //16
	manual->colour(colour);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size / 2, size / 2, size / 2); //17
	manual->colour(colour);
	manual->normal(Vector3(0, 1, 0));
	manual->position(size / 2, size / 2, -size / 2); //18
	manual->colour(colour);
	manual->normal(Vector3(0, 1, 0));
	manual->position(-size / 2, size / 2, -size / 2); //19
	manual->colour(colour);
	manual->normal(Vector3(0, 1, 0));

	//bottom
	manual->position(-size / 2, -size / 2, -size / 2); //20
	manual->colour(colour);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size / 2, -size / 2, -size / 2); //21
	manual->colour(colour);
	manual->normal(Vector3(0, -1, 0));
	manual->position(size / 2, -size / 2, size / 2); //22
	manual->colour(colour);
	manual->normal(Vector3(0, -1, 0));
	manual->position(-size / 2, -size / 2, size / 2); //23
	manual->colour(colour);
	manual->normal(Vector3(0, -1, 0));


	//front
	manual->index(0);
	manual->index(1);
	manual->index(2);

	manual->index(0);
	manual->index(2);
	manual->index(3);

	//back
	manual->index(5);
	manual->index(7);
	manual->index(6);

	manual->index(5);
	manual->index(4);
	manual->index(7);

	//left
	manual->index(12);
	manual->index(13);
	manual->index(14);

	manual->index(15);
	manual->index(14);
	manual->index(13);

	//right
	manual->index(8);
	manual->index(9);
	manual->index(10);

	manual->index(11);
	manual->index(10);
	manual->index(9);

	//top
	manual->index(16);
	manual->index(17);
	manual->index(18);

	manual->index(16);
	manual->index(18);
	manual->index(19);

	//bottom
	manual->index(20);
	manual->index(21);
	manual->index(22);

	manual->index(20);
	manual->index(22);
	manual->index(23);

	manual->end();
	mNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(manual);
}



void Planet::setPosition(float x, float y, float z)
{
	mNode->translate(Vector3(x, y, z));
}

Vector3 Planet::getPosition()
{
	return mNode->getPosition();
}

void Planet::setRevolutionSpeed(float revolutionSpd)
{
	mRevolutionSpd += revolutionSpd;
}

void Planet::setParent(Planet* parent) //Not Working
{
	mParent = parent;
	parentNode = mParent->getNode();

	//parentNode = parent->getNode();
	//parentNode->setPosition(parent->getPosition().x, parent->getPosition().y, parent->getPosition().z);
	//blank at the moment but this function is for the MOON only 
}

SceneNode* Planet::getNode()
{
	return mNode;
}

float Planet::getRevolutionSpd()
{
	return mRevolutionSpd;
}


void Planet::update(const FrameEvent& evt)
{
	Degree revolutionDegrees = Degree(mRevolutionSpd * evt.timeSinceLastFrame);

	//for MOON
	float oldX = mNode->getPosition().x - mParent->getNode()->getPosition().x; //MOON NOT REVOLVING AROUND EARTH
	float oldZ = mNode->getPosition().z - mParent->getNode()->getPosition().z; //IDK WHERE MOON IS REVOLVING

	float newX = (oldX * Math::Cos(Radian(revolutionDegrees))) + (oldZ * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (oldX * -(Math::Sin(Radian(revolutionDegrees)))) + (oldZ * Math::Cos(Radian(revolutionDegrees)));

	newX += mParent->getNode()->getPosition().x;
	newZ += mParent->getNode()->getPosition().z;

	mNode->setPosition(newX, mNode->getPosition().y, newZ); 

	


	//make planets rotate at Y axis
	Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	mNode->rotate(Vector3(1, 1, 0), rotationAngle);
}