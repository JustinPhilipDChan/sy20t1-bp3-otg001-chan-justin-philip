/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	//earth is 6 degrees per second (365 days)
	//mercury revolves around the sun in 88 days, mercury revolves 4.15 faster than earth (365 / 88)
	//using that logic, calculate the rest of the planets

	//light
	//light is at 0,0 while every cube is revolving around 0,0 except for SUN which is 0,0 itself so it has no light
	//rotating at X and Y to see all faces of the cube
	//seems to be working well but all im getting is the color yellow

	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	pointLight->setCastShadows(false);


	sun = new Planet(mSceneMgr, 20, ColourValue(1, 1, 0));
	sun->setParent(sun);

	mercury = new Planet(mSceneMgr, 3, ColourValue(0, 1, 0));
	mercury->getNode()->setPosition(20, 0, 0);
	mercury->setParent(sun);
	mercury->setRevolutionSpeed(24.9f);

	venus = new Planet(mSceneMgr, 5, ColourValue(0.65f, 0.79f, 0.95f));
	venus->getNode()->setPosition(35, 0, 0);
	venus->setParent(sun);
	venus->setRevolutionSpeed(9.78f);

	earth = new Planet(mSceneMgr, 10, ColourValue(1, 1, 1));
	earth->getNode()->setPosition(57, 0, 0);
	earth->setParent(sun);
	earth->setRevolutionSpeed(6); // every thing is correct :)

	moon = new Planet(mSceneMgr, 1, ColourValue(0.55f, 1, 0.2f));
	moon->getNode()->setPosition(65, 0, 0);
	moon->setParent(earth);
	moon->setRevolutionSpeed(12);

	mars = new Planet(mSceneMgr, 8, ColourValue(1, 0, 0));
	mars->getNode()->setPosition(78, 0, 0);
	mars->setParent(sun);
	mars->setRevolutionSpeed(3.18f);
}

//forgot to save this file as Exercise 3 Rotation, the create cube function was already implemented in the Planet function which I will pass in a seperate file
//sorry if this is confusing sir 

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	sun->update(evt);
	mercury->update(evt);
	venus->update(evt);
	earth->update(evt);
	moon->update(evt);
	mars->update(evt);





	// Below are the codes from the previous exercises

	// { Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
	//	cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);

	//Degree revolutionDegrees = Degree(30 * evt.timeSinceLastFrame);

	//float newX = (cubeNode->getPosition().x * Math::Cos(Radian(revolutionDegrees))) + (cubeNode->getPosition().z * Math::Sin(Radian(revolutionDegrees)));
	//float newZ = (cubeNode->getPosition().x * -(Math::Sin(Radian(revolutionDegrees)))) + (cubeNode->getPosition().z * Math::Cos(Radian(revolutionDegrees)));

	//cubeNode->setPosition(newX, cubeNode->getPosition().y, newZ);

	////ROTATION

	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8)) //X+
	//{
	//	cubeNode->rotate(Vector3(1, 0, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2)) //X-
	//{
	//	cubeNode->rotate(Vector3(-1, 0, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6)) //Y+
	//{
	//	cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)) //Y-
	//{
	//	cubeNode->rotate(Vector3(0, -1, 0), rotationAngle);
	//}

	////STOP ROTATION
	//if (!(mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)))
	//{
	//	cubeNode->rotate(Vector3(0, 0, 0), rotationAngle);
	//}



	////MOVEMENT
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
	//{
	//	//cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame , 0.0, 0.0);
	//	//mAcceleration = mAcceleration + 0.2;
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;

	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	////diagonal input
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	//}
	//if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	//	(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	//}

	//// if nothing is pressed, reset acceleration
	//if (!mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_I) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	//{
	//	velocity = Vector3::ZERO;
	//}


	//speed++;
	//cubeNode->translate(velocity);
	//}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
