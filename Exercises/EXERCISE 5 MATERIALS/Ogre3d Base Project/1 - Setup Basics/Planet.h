#pragma once
#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>

using namespace Ogre;

class Planet 
{
public:
	Planet(SceneManager* SceneMgr, float size, ColourValue colour, std::string name);

	

	//setters and getters
	void setPosition(float x, float y, float z);
	Vector3 getPosition();
	void setRevolutionSpeed(float revolutionSpd);
	float getRevolutionSpd();
	void setParent(Planet* parent);
	SceneNode* getNode();

	void update(const FrameEvent& evt);
	
private:
	SceneNode* mNode;
	SceneNode* parentNode;
	SceneManager* mSceneMgr;

	Planet* mParent;

	float mRevolutionSpd;
};

