/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	ManualObject* cube = createCube();
	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(cube);
	
}

//forgot to save this file as Exercise 3 Rotation, the create cube function was already implemented in the Planet function which I will pass in a seperate file
//sorry if this is confusing sir 

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{

	{Radian rotationAngle = Radian(Degree(30 * evt.timeSinceLastFrame));
		cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);

	Degree revolutionDegrees = Degree(30 * evt.timeSinceLastFrame);

	float newX = (cubeNode->getPosition().x * Math::Cos(Radian(revolutionDegrees))) + (cubeNode->getPosition().z * Math::Sin(Radian(revolutionDegrees)));
	float newZ = (cubeNode->getPosition().x * -(Math::Sin(Radian(revolutionDegrees)))) + (cubeNode->getPosition().z * Math::Cos(Radian(revolutionDegrees)));

	cubeNode->setPosition(newX, cubeNode->getPosition().y, newZ);

	//ROTATION

	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8)) //X+
	{
		cubeNode->rotate(Vector3(1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2)) //X-
	{
		cubeNode->rotate(Vector3(-1, 0, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6)) //Y+
	{
		cubeNode->rotate(Vector3(0, 1, 0), rotationAngle);
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)) //Y-
	{
		cubeNode->rotate(Vector3(0, -1, 0), rotationAngle);
	}

	//STOP ROTATION
	if (!(mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6) || mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4)))
	{
		cubeNode->rotate(Vector3(0, 0, 0), rotationAngle);
	}



	//MOVEMENT
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
	{
		//cubeNode->translate(10.0 * mAcceleration * evt.timeSinceLastFrame , 0.0, 0.0);
		//mAcceleration = mAcceleration + 0.2;
		(velocity.x += 10 * evt.timeSinceLastFrame)* speed;

	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
	{
		(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	}
	//diagonal input
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
		(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		(velocity.x += 10 * evt.timeSinceLastFrame)* speed;
		(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
		(velocity.y += 10 * evt.timeSinceLastFrame)* speed;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		(velocity.x -= 10 * evt.timeSinceLastFrame)* speed;
		(velocity.y -= 10 * evt.timeSinceLastFrame)* speed;
	}

	// if nothing is pressed, reset acceleration
	if (!mKeyboard->isKeyDown(OIS::KeyCode::KC_J) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_I) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_L) && !mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		velocity = Vector3::ZERO;
	}


	speed++;
	cubeNode->translate(velocity);
	}
	return true;
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
